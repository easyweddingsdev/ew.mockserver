﻿using Topshelf;

namespace ew.MockServer
{
    public class MockServer
    {
        static void Main(string[] args)
        {
            HostFactory.Run(hostConfig =>
            {
                hostConfig.Service<MockServerService>(serviceConfig =>
                {
                    serviceConfig.ConstructUsing(() => new MockServerService());
                    serviceConfig.WhenStarted(s => s.Start());
                    serviceConfig.WhenStopped(s => s.Stop());
                });
            });

        }
    }
}