﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ew.MockServer
{
    public class ServerLogEntry
    {
        [Key]
        public Guid Id { get; set; }

        public string AbsoluteUrl { get; set; }
        public string Body { get; set; }
        public string Method { get; set; }
        public DateTime CreatedDateUtc { get; set; }
    }
}
