﻿using Microsoft.EntityFrameworkCore;

namespace ew.MockServer
{
    public class LoggingContext : DbContext
    {
        public LoggingContext()
        {
            Database.EnsureCreated();
        }

        public DbSet<ServerLogEntry> ServerLogEntries { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./log.db");
        }
    }
}
