﻿using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using WireMock;
using WireMock.Logging;
using WireMock.Net.StandAlone;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using WireMock.Settings;

namespace ew.MockServer
{
    public class MockServerService
    {
        private FluentMockServer _server;

        public void Start()
        {
            // see source code for all the possible properties
            var settings = new FluentMockServerSettings
            {
                AllowPartialMapping = true,
                StartAdminInterface = true,
                Port = 3337
            };

            _server = StandAloneApp.Start(settings);

            _server
               .Given(
                 Request.Create()
                   .WithPath("/*")
                   .UsingGet()
               )
              .AtPriority(1)
               .RespondWith(
                 Response.Create()
                   .WithProxy("https://onesignal.com")
               );

            _server
              .Given(
                Request.Create()
                  .WithPath(path => path.StartsWith("/logs"))
                  .UsingGet()
              )
              .AtPriority(0)
              .RespondWith(
                Response.Create().WithBody(request => GetLogEntriesJson(request)));

            _server
                .Given(Request.Create().UsingPost())
                .WithPath("api/v1/notifications")
                .RespondWith
                   (Response.Create().WithBodyAsJson(new { id = Guid.NewGuid(), recipients = 5,  external_id = default(int?) }));

            _server.LogEntriesChanged += Server_LogEntriesChanged;
        }

        private static string GetLogEntriesJson(RequestMessage request)
        {
            using (var context = new LoggingContext())
            {
                List<ServerLogEntry> results = new List<ServerLogEntry>();

                DateTime startDateUtc;

                results = context.ServerLogEntries.ToList();

                if (request.Query.ContainsKey("startDateUtc") && DateTime.TryParse(request.Query["startDateUtc"].FirstOrDefault(), out startDateUtc))
                {
                    results = results.Where(s => startDateUtc < s.CreatedDateUtc).ToList();
                }

                HttpMethod method;

                if (Enum.TryParse(request.PathSegments.Last(), true, out method))
                {
                    results = results.Where(s => s.Method.Equals(method.ToString(), StringComparison.InvariantCultureIgnoreCase)).ToList();
                }

                string search = null;

                if (request.Query.ContainsKey("search"))
                {
                    search = request.Query["search"].FirstOrDefault();
                    results = results.Where(s => s.Body != null && s.Body.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToList();
                }

                return JsonConvert.SerializeObject(results);
            }
        }
        private static void Server_LogEntriesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            using (var context = new LoggingContext())
            {
                foreach (LogEntry serverRequest in e.NewItems)
                {
                    context.ServerLogEntries.Add(new ServerLogEntry
                    {
                        Id = serverRequest.Guid,
                        AbsoluteUrl = serverRequest.RequestMessage.AbsoluteUrl,
                        Body = serverRequest.RequestMessage.Body,
                        Method = serverRequest.RequestMessage.Method,
                        CreatedDateUtc = DateTime.UtcNow
                    });
                }

                context.SaveChanges();
            }
        }


        public void Stop()
        {
            _server.Stop();
        }
    }
}
